import {ActionTree, GetterTree, MutationTree} from 'vuex';
import Product from "~/models/Product";

export class RootState {
  public products: Array<Product> = [];
  public currentProduct: Product | null = null;
}

export const state = () => new RootState();

export const mutations = {
  SET_PRODUCTS(state, products: Array<Product>) {
    state.products = products;
  },
  SET_CURRENT_PRODUCT(state, productId: string) {
    const product = state.products.find((prod) => prod.id === productId);
    state.currentProduct = product || null;
  },
  CREATE_PRODUCT(state, newProduct: Product) {
    state.products.push(newProduct);
  },
  REMOVE_PRODUCT(state, productId: string) {
    const productIndex: number = state.products.findIndex((prod) => prod.id === productId);
    state.products.splice(productIndex, 1);
  },
  UPDATE_PRODUCT(state, updatedProduct: Product) {
    const productIndex: number = state.products.findIndex((prod) => prod.id === updatedProduct.id);
    state.products.splice(productIndex, 1, updatedProduct);
  }
} as MutationTree<RootState>;

export const actions = {
  async nuxtServerInit({ commit, state }) {
    const res = await fetch('http://localhost:3000/data.json');
    const data = await res.json();
    commit('SET_PRODUCTS', data);
  }
} as ActionTree<RootState, RootState>;

export const getters = {
  products: ({ products }) => products,
  currentProduct: ({ currentProduct }) => currentProduct
} as GetterTree<RootState, RootState>;
