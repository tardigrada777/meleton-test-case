import { v4 as uuidv4 } from 'uuid';

export default class Product {
  constructor(
    public id: string,
    public name: string,
    public price: number,
    public date: string
  ) {}

  public static default() {
    const rawDate = new Date();
    const year = rawDate.getFullYear();
    const month = rawDate.getMonth() < 10 ? `0${rawDate.getMonth()}` : rawDate.getMonth();
    const day = rawDate.getDate() < 10 ? `0${rawDate.getDate()}` : rawDate.getDate();
    const date = `${year}-${month}-${day}`;
    return new Product(uuidv4(), 'default', 0, date);
  }
}
